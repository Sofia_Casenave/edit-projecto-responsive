//JQuery lightSlider Plugin

$(document).ready(function () {
  var slider = $("#content-slider").lightSlider({
    loop: true,
    keyPress: true,
    auto: true,
    slideMove: 1,
    item: 5,
    pauseOnHover: true,
    slideMargin: 0.1,
    speed: 400,
    mode: "slide",
    controls: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          item: 4,
          slideMove: 1,
          slideMargin: 0.1,
        },
      },
      {
        breakpoint: 800,
        settings: {
          item: 3,
          slideMove: 1,
          slideMargin: 0.1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          item: 2,
          slideMove: 1,
        },
      },
      {
        breakpoint: 450,
        settings: {
          item: 1,
          slideMove: 1,
        },
      },
    ],
  });
  $(".slideshow-btn-prev").click(function () {
    slider.goToPrevSlide();
  });
  $(".slideshow-btn-next").click(function () {
    slider.goToNextSlide();
  });

  //Click event to show/hide the Input Search

  $("a.search").click(function (e) {
    e.preventDefault();
    $(".inputSearch").toggle();
  });

  //Click / tap event to toggle the Menu's visibility

  $(".mobile-menu").click(function (e) {
    $(".left-menu").slideToggle(400);
  });

  //Change Navbar (menu) and add a CSS class on Scroll

  $(function () {
    var header = $("header");

    $(window).scroll(function () {
      var scroll = $(window).scrollTop();
      if (scroll >= 50) {
        header.addClass("scrolled");
      } else {
        header.removeClass("scrolled");
      }
    });
  });

  //Change bigger image by clicking on thumbnail images

  $(function () {
    $(".single-thumbnail a").click(function (e) {
      var href = $(this).attr("href");
      $(".bigger img").attr("src", href);
      e.preventDefault();
      return false;
    });
  });
});
